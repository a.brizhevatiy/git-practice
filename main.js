function multipleFunction(selector, ivent, ...functions) {
    const elem = document.querySelector(selector);
    functions.forEach(func, function () {
        func(elem);
    })
}